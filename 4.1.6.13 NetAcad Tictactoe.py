import random
def DisplayBoard(board):
#
# the function accepts one parameter containing the board's current status
# and prints it out to the console
#
    def secondrow():
        for j in range(3):
            print('|' , ' '*7 , sep='', end = '') #second row dash board
        else:
            print('|')

    for i in range(3):
        print('+','-'*7,'+','-'*7,'+','-'*7,'+',sep='') #first row dashboard
        secondrow()
        for j in range(3): #third row dash board
            print('|' , ' '*3, board[i][j] , ' '*3 , sep='', end='')
        else:
            print('|')
        secondrow()
    else:
        print('+','-'*7,'+','-'*7,'+','-'*7,'+',sep='')

def boardfilling(argv,argc):
    # print(argv,argc) #testing errors
    position = dictboard[argv]
    # print(position)
    x = board[position[0]][position[1]] = argc
    # print(x)

def EnterMove(board):
    #the function accepts the board current status
    if VictoryFor(board, computer):
        return True
    # asks the user about their move
    # checks the input
    while True:
        try:
            move = int(input("Select a position number in the board: "))
            if move in filledlist:
                print("\n----------This place is already filled---------\n")
                continue
            elif (move>0 and move <10):
                break
            else:
                print("\n\nInput a correct number!\n\n")
        except(ValueError, NameError,TypeError):
            print("\nInput a correct position number!\n\n")

    # updates the board according to the user's decision
    boardfilling(move, user)
    filledlist.append(move)
    return None

def MakeListOfFreeFields(board):
# the function browses the board and builds a list of all the free squares;
# the list consists of tuples, while each tuple is a pair of row and column numbers
    board = [[0 for i in range(3)] for j in range(3)]
    tempnum = 0 #temporary number
    for i in range(len(board)):
        for j in range(len(board)):
            tempnum+=1
            board[i][j]=tempnum
            dictboard[tempnum]= (i,j)
    return board


def VictoryFor(board, sign):
# the function analyzes the board status in order to check if
# the player using 'O's or 'X's has won the game
    for i in range(len(board)):
        if board[i][0]==board[i][1]==board[i][2]==sign or\
        board[0][i]==board[1][i]==board[2][i]==sign:
            return True
        if board[0][0]==board[1][1]==board[2][2]==sign or\
        board[0][2]==board[1][1]==board[2][0]==sign:
            return True



def DrawMove(board):
    # the function draws the computer's move
    if VictoryFor(board, user):
        return True
    elif board[1][1]!= computer:
        move = 5
        boardfilling(move, computer)
        filledlist.append(move)
        return None
    else:
        while True:
            move=random.randrange(1,10)
            if move in filledlist:
                continue
            else:
                boardfilling(move,computer)
                filledlist.append(move)
                break



board = []
computer = 'X'
user = 'O'
win = 0
loose = 0
draw = 0
ending = 'total win = {} and total loose = {}'
while True:
    dictboard = {}
    filledlist = [] #list containing already filled cell
    board = MakeListOfFreeFields(board)
    # print(board)
    while True:
        print("\n\n")
        DisplayBoard(board)
        print("\n\nComputer's turn\n\n")
        #x = DrawMove(board) # computer draw
        if DrawMove(board)==True:
            print("\n\n\n-------------YOU WIN!-------------")
            win+=1
            break
        print("\n\nYour turn\n\n")
        DisplayBoard(board)
        if len(filledlist) == 9:
            if VictoryFor(board,computer):
                print("\n\n\n-------------YOU LOOSE!-------------\n\n")
                loose+=1
                break
            else:
                print("\n\n-------------DRAW-------------\n\n")
                draw+=1
                break
        print("\n\n")
        #y = EnterMove(board) # user draw
        if EnterMove(board)==True:
            print("\n\n\n-------------YOU LOOSE!-------------\n\n")
            loose+=1
            break
        

    x = input("Do you want to play again?(Y/N)\n")
    if x.upper() == 'Y':
        continue
    else:
        break
print('total win = {} and total loose = {} and total draw = {}'.format(win,loose,draw))
print("\n\n Done")
